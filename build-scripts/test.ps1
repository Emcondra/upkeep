node node_modules\@angular\cli\bin\ng lint
if ($lastexitcode -ne 0) {
    throw "lint failed"
}
node node_modules\@angular\cli\bin\ng test --progress false --watch false
if ($lastexitcode -ne 0) {
    throw "test failed"
}