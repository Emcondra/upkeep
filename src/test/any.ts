export class Any {
    public static string(length: number, charMask = 'aA#!'): string {
        let mask = '';
        if (charMask.indexOf('a') > -1) {
            mask += 'abcdefghijklmnopqrstuvwxyz';
        }
        if (charMask.indexOf('A') > -1) {
            mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if (charMask.indexOf('#') > -1) {
            mask += '0123456789';
        }
        if (charMask.indexOf('!') > -1) {
            mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        }
        if (charMask === 'hex') {
            mask += '0123456789ABCDEF';
        }

        const result = new Array<string>(length);
        const maskLength = mask.length;

        for (let i = length; i > 0; --i) {
            result[i] = mask[Math.floor(Math.random() * maskLength)];
        }

        return result.join('');
    }

    public static bool(): boolean {
        const num = Any.int(0, 1000) % 2;

        return num === 1;
    }

    public static int(min = 0, max = 1000): number {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    public static noOpSubscriber(): () => void {
        return () => {/*intentionally empty*/
        };
    }

    public static errorMessage(): string {
        return Any.string(100);
    }
}
