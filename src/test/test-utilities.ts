import { ComponentFixture } from '@angular/core/testing';

export class TestUtilities {
    public static getAttribute(selector: string, attribute: string, fixture: ComponentFixture<any>): any {
        const element = fixture.nativeElement.querySelector(selector);

        expect(element).toExist(`TestUtilities.getAttribute: Expected element to exist, ${selector}`);

        return element.getAttribute(attribute);
    }

    public static getButtonValue(selector: string, fixture: ComponentFixture<any>): string {
        const element = fixture.nativeElement.querySelector(selector);

        expect(element).toExist(`TestUtilities.getAttribute: Expected element to exist, ${selector}`);

        return element.value;
    }

    public static isElementEnabled(selector: string, fixture: ComponentFixture<any>): boolean {
        const element = fixture.nativeElement.querySelector(selector);
        expect(element).toExist(`TestUtilities.isElementEnabled: Expected element to exist, ${selector}`);

        return !element.disabled;
    }

    public static isElementHidden(selector: string, fixture: ComponentFixture<any>): boolean {
        const element = fixture.nativeElement.querySelector(selector);
        expect(element).toExist(`TestUtilities.isElementHidden: Expected element to exist, ${selector}`);

        const computedStyle = window.getComputedStyle(element);

        return (computedStyle.display === 'none' || computedStyle.visibility === 'hidden');
    }

    public static click(selector: string, fixture: ComponentFixture<any>): void {
        expect(this.isElementEnabled(selector, fixture)).toBeTruthy(`${selector} is not clickable`);
        expect(this.isElementHidden(selector, fixture)).toBeFalsy('Element is hidden and cannot be clicked');

        const element = fixture.nativeElement.querySelector(selector);
        expect(element).toExist(`TestUtilities.click: Expected clickable element to exist, ${selector}`);

        element.dispatchEvent(new Event('click'));

        fixture.detectChanges();
    }

    public static setTextboxValue(selector: string, value: any, fixture: ComponentFixture<any>): void {
        const element = fixture.nativeElement.querySelector(selector);

        expect(element).toExist(`TestUtilities.setTextboxValue: Expected textbox to exist, ${selector}`);

        element.value = value;
        element.dispatchEvent(new Event('input'));
        fixture.detectChanges();
    }

    public static submitForm(selector: string, fixture: ComponentFixture<any>): void {
        const element = fixture.nativeElement.querySelector(selector);

        expect(element).toExist(`TestUtilities.submitForm: Expected element to exist, ${selector}`);
        element.dispatchEvent(new Event('submit'));
        fixture.detectChanges();
    }

    public static getElementInnerText(selector: string, fixture: ComponentFixture<any>): string {
        const element = fixture.nativeElement.querySelector(selector);

        expect(element).toExist(`TestUtilities.getElementInnerText: Expected element to exist, ${selector}`);
        return element.innerText.trim();
    }
}
