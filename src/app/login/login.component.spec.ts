import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestUtilities } from '../../test/test-utilities';
import { LoginService } from '../login.service';
import { Any } from '../../test/any';
import { Router } from '@angular/router';
import { throwError, of } from 'rxjs';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            imports: [
                FormsModule,
                ReactiveFormsModule
            ],
            providers: [
                { provide: LoginService, useValue: jasmine.createSpyObj('LoginService', ['login']) },
                { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate']) }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should indicate to the user where to enter the username', () => {
        expect(TestUtilities.getAttribute('#username-input', 'placeholder', fixture)).toEqual('Username');
    });

    it('should indicate to the user where to enter the password', () => {
        expect(TestUtilities.getAttribute('#password-input', 'placeholder', fixture)).toEqual('Password');
    });

    it('should display a login button', () => {
        expect(TestUtilities.getButtonValue('#login-button', fixture)).toEqual('Login');
    });

    it('should attempt to log the user in when the login button is clicked', () => {
        const username = Any.string(10);
        const password = Any.string(10);

        TestUtilities.setTextboxValue('#username-input', username, fixture);
        TestUtilities.setTextboxValue('#password-input', password, fixture);

        const loginServiceMock = TestBed.get(LoginService);
        loginServiceMock.login.and.returnValue(of(Any.noOpSubscriber()));

        TestUtilities.submitForm('form', fixture);

        expect(loginServiceMock.login).toHaveBeenCalledOnceWith(username, password);
    });

    it('should route to the landing page after successful login', () => {
        TestUtilities.setTextboxValue('#username-input', Any.string(10), fixture);
        TestUtilities.setTextboxValue('#password-input', Any.string(10), fixture);

        const loginServiceStub = TestBed.get(LoginService);
        loginServiceStub.login.and.returnValue(of(true));

        TestUtilities.submitForm('form', fixture);

        const routerMock = TestBed.get(Router);

        expect(routerMock.navigate).toHaveBeenCalledOnceWith(['landing']);
    });

    it('should display invalid login message for a bad login', () => {
        const expectedErrorMessage = Any.errorMessage();
        TestUtilities.setTextboxValue('#username-input', Any.string(10), fixture);
        TestUtilities.setTextboxValue('#password-input', Any.string(10), fixture);

        const loginServiceMock = TestBed.get(LoginService);
        loginServiceMock.login.and.returnValue(throwError(expectedErrorMessage));

        TestUtilities.submitForm('form', fixture);

        expect(TestUtilities.getElementInnerText('#login-error', fixture)).toEqual(expectedErrorMessage);
    });

    it('should disable the login button if user does not supply a password', () => {
        TestUtilities.setTextboxValue('#username-input', Any.string(10), fixture);

        expect(TestUtilities.isElementEnabled('#login-button', fixture)).toBeFalsy('login button should be disabled');
    });

    it('should disable the login button if user does not supply a username', () => {
        TestUtilities.setTextboxValue('#password-input', Any.string(10), fixture);

        expect(TestUtilities.isElementEnabled('#login-button', fixture)).toBeFalsy('login button should be disabled');
    });
});
