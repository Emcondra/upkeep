import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {

    public loginForm: FormGroup;
    public errorMessage: string;

    constructor(private _loginService: LoginService, private _router: Router) {
        this.loginForm = new FormGroup({
            'username': new FormControl('', Validators.required),
            'password': new FormControl('', Validators.required)
        });
    }

    public login(): void {
        const username = this.loginForm.controls['username'].value;
        const password = this.loginForm.controls['password'].value;
        this._loginService.login(username, password)
            .subscribe(isValidLogin => {
                if (isValidLogin) {
                    this._router.navigate(['landing']);
                }
            }, errorMessage => this.errorMessage = errorMessage);
    }
}
