import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class LoginService {

    constructor(private _http: HttpClient) { }

    public login(username: string, password: string): Observable<boolean> {
        const headers = new HttpHeaders().set('Authorization', this.prepareCredentialsForHeader(username, password));
        return this._http.get<any>('https://atlasrfid.atlassian.net/rest/api/2/myself', { headers })
            .pipe(
                map(m => m.active),
                catchError(e => throwError(e.error.message))
            );
    }

    private prepareCredentialsForHeader(username: string, password: string): string {
        const credentials = btoa(`${username}:${password}`);
        return `Basic ${credentials}`;
    }
}
