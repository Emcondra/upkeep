import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { LoginService } from './login.service';
import { HttpRequest } from '@angular/common/http';
import { Any } from '../test/any';

describe('LoginService', () => {
    let httpTestingController: HttpTestingController;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [LoginService]
        });

        httpTestingController = TestBed.get(HttpTestingController);
    });

    it('should be created', inject([LoginService], (service: LoginService) => {
        expect(service).toBeTruthy();
    }));

    it('should indicate if the user\'s credentials are valid', inject([LoginService], (service: LoginService) => {
        const expectedResponse = { active: Any.bool() };

        let actualResult;
        service.login('username', 'password').subscribe(result => actualResult = result);

        httpTestingController.expectOne((request: HttpRequest<any>) => {
            return request.url === 'https://atlasrfid.atlassian.net/rest/api/2/myself' &&
                request.method === 'GET' &&
                request.headers.get('Authorization') === 'Basic dXNlcm5hbWU6cGFzc3dvcmQ=';
        })
            .flush(expectedResponse);
        httpTestingController.verify();

        expect(actualResult).toEqual(expectedResponse.active);
    }));

    it('should catch and return the error message when error occurs', inject([LoginService], (service: LoginService) => {
        const expectedErrorMessage = Any.errorMessage();

        let actualResult;
        service.login('username', 'password').subscribe(null, result => actualResult = result);

        httpTestingController.expectOne('https://atlasrfid.atlassian.net/rest/api/2/myself')
            .flush({ message: expectedErrorMessage }, { status: 500, statusText: 'Bad Request' });
        httpTestingController.verify();

        expect(actualResult).toEqual(expectedErrorMessage);
    }));
});
